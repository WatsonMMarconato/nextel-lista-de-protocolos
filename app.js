(function() {
  return {
    events: {
      'app.activated':'showProtocols',
      'getProtocols.done' : 'protocolsDone',
      'getProtocols.fail' : 'fail',

    },

    requests: { 
		getProtocols: function() { 
			return { 
				url: 'https://nextel.staging.gateway.zup.me/rw/v1/protocols', 
				type: 'GET',
				dataType: 'json',
				headers : {
					'X-Organization-Slug' : 'nextel',
					'X-Application-Id':'0da14c95e15c5cbbc1cd3d06c238fe430c0adcfe',
					'X-Application-Key':'19959d306c180134b2eb021e75abe44c'
				} 
			}; 
		}
	},

    showProtocols: function() {
    	this.ajax('getProtocols');
    },
    protocolsDone: function(data){
    	var objListProtocol = {protocols : data}
    	this.switchTo('protocol', objListProtocol);
    },
    fail: function(data) {
      console.log(data);
      services.notify(JSON.stringify(data));
    }
  };

}());
